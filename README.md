using System;

class MainClass {
  public static void Main (string[] args) {
    
    double alpha=1;

    double x1=1.0;
    double x2=0.0;

    Console.WriteLine("Enter the coefficent");
    double c=double.Parse(Console.ReadLine());
    double w1=0.45*c;
    double w2=0.78*c;
    double w3=-0.12*c;
    double w4=0.13*c;
    double w5=1.5*c;
    double w6=-2.3*c;

    double h1input=x1*w1 +x1*w3;
    double h1output =1.0 / (1.0 + Math.Pow(Math.E, -alpha*h1input));
    Console.WriteLine("H1 output" + h1output);

    double h2input=x1*w2 +x1*w4;
    double h2output =1.0 / (1.0 + Math.Pow(Math.E, -alpha*h2input));
    Console.WriteLine("H2 output" + h2output);

    double o1input=h1output*w5 +h2output*w6;
    double o1output =1.0 / (1.0 + Math.Pow(Math.E, -alpha*o1input));
    Console.WriteLine("O1 output" + o1output);
     
  }
  
}